package shops;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shops.models.Book;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BasketTest {
    Basket basket;

    @BeforeEach
    void beforeEach(){
        basket = new Basket();
    }

    @Test
    void shouldGetAllBooks() {
        //given
        Book book1 = new Book("Harry Potter", "J.K. Rowling", 30);
        Book book2 = new Book("Clean Code", "Robert Cecil Martin", 20);
        basket.addBook(book1);
        basket.addBook(book2);

        //when
        List<Book> retrievedBooks = basket.getAllBooks();

        //then
        assertNotNull(retrievedBooks);
        assertEquals(2, retrievedBooks.size());
        assertEquals("Harry Potter", retrievedBooks.get(0).getTitle());
        assertEquals("Clean Code", retrievedBooks.get(1).getTitle());
    }

    @Test
    void shouldGetNoBooks() {
        //given
        //when
        List<Book> retrievedBooks = basket.getAllBooks();

        //then
        assertEquals(0, retrievedBooks.size());
    }

    @Test
    void shouldGetNoBooksNull() {
        //given
        //when
        List<Book> retrievedBooks = basket.getAllBooks();

        //then
        assertEquals(0, retrievedBooks.size());
    }

    @Test
    void shouldAddBook() {
        //given
        Book book1 = new Book("Harry Potter", "J.K. Rowling", 30);

        //when
        basket.addBook(book1);

        //then
        assertNotNull(basket.getAllBooks());
        assertEquals(1, basket.getAllBooks().size());
        assertEquals("Harry Potter", basket.getAllBooks().get(0).getTitle());
    }

    @Test
    void shouldNotAddBookWhenNull() {
        //given

        //when
        basket.addBook(null);

        //when
        assertEquals(0, basket.getAllBooks().size());
    }

    @Test
    void shouldClearBasket() {
        //given
        Book book1 = new Book("Harry Potter", "J.K. Rowling", 30);
        basket.addBook(book1);

        //when
        basket.clear();

        //then
        assertEquals(0, basket.getAllBooks().size());
    }

    @Test
    void shouldDoNothingWhenClearEmptyBasket() {
        //given

        //when
        basket.clear();

        //then
        assertEquals(0, basket.getAllBooks().size());
    }

    @Test
    void shouldSumPricesWhenOneBook() {
        //given
        Book book1 = new Book("Harry Potter", "J.K. Rowling", 30);
        basket.addBook(book1);

        //when
        int sum = basket.sumPrices();

        //then
        assertEquals(30, sum);
    }

    @Test
    void shouldSumPricesWhenMultipleBooks() {
        //given
        Book book1 = new Book("Harry Potter", "J.K. Rowling", 30);
        Book book2 = new Book("Clean Code", "Robert Cecil Martin", 20);
        Book book3 = new Book("Katarynka", "Bolesław Prus", 10);
        basket.addBook(book1);
        basket.addBook(book2);
        basket.addBook(book3);

        //when
        int sum = basket.sumPrices();

        //then
        assertEquals(60, sum);
    }

}
